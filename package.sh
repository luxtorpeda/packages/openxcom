#!/bin/bash

source env.sh

set -x

# Create a tarball, in a reproducible way

for app_id in $STEAM_APP_ID_LIST ; do
	pushd "$app_id" || exit 1
	# FIXME tar invocation in format newer than v7 results
	# in non-reproducible package - these formats embed
	# machine-specific information inside.
	# However, we can't use v7 for now, because files created
	# by OpenXcom are longer than 99 characters.
	#
	# shellcheck disable=SC2046
	tar \
		--mode='a+rwX,o-w' \
		--owner=0 \
		--group=0 \
		--mtime='@1560859200' \
		-cf dist.tar \
		manifest $(list_dist) || exit 1
	xz dist.tar
	sha1sum dist.tar.xz
	popd || exit 1
done
