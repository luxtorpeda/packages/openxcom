OpenXcom package for Luxtorpeda

## Components

Submodule `source` contains source code for Luxtorpeda's mirror of
[OpenXcom](https://github.com/OpenXcom/OpenXcom) repo.
Covered by GPLv3 license.

Directory `SDL_gfx-*` contains source code for
[SDL\_gfx library](http://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/),
with following directories removed: "Other Builds", "Doc", and "Test".
You can download full source code from
[author's page](http://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/)
or
[SourceForge](https://sourceforge.net/projects/sdlgfx/files/).
Covered by ZLIB license.

Directory `yaml-cpp-*` contains source code for
https://github.com/jbeder/yaml-cpp with following directories removed: "test".
Covered by MIT license.
