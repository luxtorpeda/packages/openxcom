#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

# build SDL_gfx
#
readonly pfx="$PWD/local"
mkdir "$pfx"
pushd "SDL_gfx-2.0.26" || exit 1
./configure --prefix="$pfx"
make -j "$(nproc)"
make install
popd || exit 1

export SDLGFXDIR="$pfx"


# build yaml-cpp
#
pushd "yaml-cpp-0.6.2" || exit 1
mkdir build
cd build || exit 1
cmake \
	-DCMAKE_INSTALL_PREFIX="$pfx" \
	-DYAML_CPP_BUILD_TESTS=OFF \
	..
make -j "$(nproc)"
make install
popd || exit 1

export YAMLCPPDIR="$pfx"

# build OpenXcom
#
readonly dist="$PWD/7760/dist"
pushd "source" || exit 1
mkdir build
cd build || exit 1
cmake \
	-DCMAKE_INSTALL_PREFIX="$dist" \
	-DCMAKE_BUILD_TYPE=Release \
	..
make -j "$(nproc)"
make install
popd || exit 1
cp -rfv "$PWD/7760/dist"/* "$PWD/7650/dist"  # TODO manifest!

# Adjust installation to work with Steam out of the box
#
rm -rfv 7650/dist/share/openxcom/{UFO,TFTD}
rm -rfv 7760/dist/share/openxcom/{UFO,TFTD}

ln -s ../../TFD TFTD
mv TFTD 7650/dist/share/openxcom

ln -s ../../XCOM UFO
mv UFO 7760/dist/share/openxcom

mkdir -p 7650/dist/lib64
mkdir -p 7760/dist/lib64
cp -v local/lib/libSDL_gfx.so* 7650/dist/lib64
cp -v local/lib/libSDL_gfx.so* 7760/dist/lib64
cp -v adjunct/openxcom.sh 7650/dist
cp -v adjunct/openxcom.sh 7760/dist
